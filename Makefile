.PHONY: build run deploy test

build:
	docker-compose build


data=$(shell pwd)/data


run:
	docker-compose up


deploy: build
	docker-compose push
	scp production.* phosphorus:/var/data/podcasts.tompaton.com/
	ssh tom@phosphorus "cd /var/data/podcasts.tompaton.com && docker-compose -f production.yml pull"


test: test-200 test-progress
	@echo Test OK


define CHECK_HTTP_STATUS
@xargs -n1 -P 10 \
       curl -o /dev/null --silent --head \
            --write-out '%{url_effective}: %{http_code}\n' < $<
endef

.PHONY: test-200
test-200: test-urls-200.txt
	$(CHECK_HTTP_STATUS) | grep -v ' 200$$' \
	&& echo '200 Failed!' && exit 1 \
	|| echo '200 OK'


url="http://localhost:5511/progress/test.json"
include auth.env
export

define PUT
@curl --silent --fail --user $$PROGRESS_AUTH -H 'Content-Type: application/json' \
      -X PUT -d $(1) $(url) \
  && echo PUT $(1) OK \
  || echo PUT $(1) Failed!
endef

define GET
@curl --silent --user $$PROGRESS_AUTH $(url) | grep -vF $(1) \
  && echo GET $(1) Failed! && exit 1 \
  || echo GET $(1) OK
endef

.PHONY: test-progress
test-progress:
	$(call PUT,"[1]")
	$(call GET,"[1]")

	$(call PUT,"[2]")
	$(call GET,"[2]")

	@echo Test progress OK
