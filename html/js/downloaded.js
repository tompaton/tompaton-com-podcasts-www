var _state = {
    "urls": {},  // guid --> url
    "date": {},  // date --> [[ {duration, title, guid}, ... ], ...]
    "selected": {"date": null, "guid": null},
    "_dom_text": {},
    "_completed": [],
    "_playing": [],
    "_dom": {"player": document.querySelector('#player audio'),
             "play_button": document.querySelector('.podcast-play'),
             "progress_bar": document.querySelector('.podcast-progress'),
             "current_time": document.querySelector('.podcast-currenttime'),
             "remain_time": document.querySelector('.podcast-remaintime'),
             "sync_status": document.querySelector('#sync-progress-status'),
             "errors": document.querySelector('#errors')},
    "tabs": {"active": 0,
             "body": [document.getElementById('priority0'),
                      document.getElementById('priority1'),
                      document.getElementById('priority2'),
                      document.getElementById('tab-sidebar')]},
    "playing": null,  // guid
    "autoplay": false,
    "timestamps": {},  // guid --> {timestamp}
    "_progress": {"timeout": null,
                  "guid": null,
                  "value": null},
    "sync": null,  // Basic auth header btoa(uid:pwd)
    "priority": make_priority([]),
};

function load_rss(rss_url, enable_sync) {
    function on_load() {
        // rss > channel
        if(this.status < 400 && this.responseXML) {
            var rss = this.responseXML.children[0].children[0];
            save_rss(this.responseText);
        } else {
            var rss = restore_rss();
            logError("RSS Error: " + this.status + " " + this.statusText + "\n");
        }
        init_rss(rss, _state);
        init_db(window.localStorage, _state, enable_sync);
        mount(_state, rss,
              document.getElementById('priority1'),
              document.getElementById('tally'));
        render(_state);
        sync_progress(_state);
        sync_priority(_state);
    }

    var oReq = new XMLHttpRequest();
    oReq.addEventListener("load", on_load);
    oReq.addEventListener("error", on_load);
    oReq.addEventListener("abort", on_load);
    oReq.open("GET", rss_url + '?cache-' + +(new Date()));
    oReq.send();
}

function reload_rss() {
    window.location.reload();
    return false;
}

function init_rss(rss, state) {
    nodes(rss, 'item').forEach(function(item) {
        var guid = value(item, 'guid');
        var pubdate = item_pubdate(item);
        var tally_title = (value(item, 'author')
                           + ': '
                           + value(item, 'title')
                           + ' ('
                           + value(item, 'itunes:duration')
                           + ')');

        state.urls[guid] = attr(item, 'enclosure', 'url');

        if(state.date[pubdate] === undefined) {
            state.date[pubdate] = make_priority([]);
        }

        state.date[pubdate][1].push({"duration": item_duration(item),
                                     "title": tally_title,
                                     "guid": guid});
    });
}

function nodes(item, tagName) {
    var result = [];
    for(var i=0; i < item.children.length; i++) {
        if(item.children[i].tagName == tagName) {
            result.push(item.children[i]);
        }
    }
    return result;
}

function value(item, tagName) {
    var n = nodes(item, tagName);
    if(n.length) {
        return n[0].textContent;
    } else {
        return '';
    }
}

function attr(item, tagName, attrName) {
    var node = nodes(item, tagName)[0];
    if(node) {
        return node.attributes[attrName].value;
    } else {
        return '';
    }
}

function item_pubdate(item) {
    var d = value(item, 'pubDate').substr(0, 16).split(' ');
    return d[2] + ' ' + d[1];
}

function item_duration(item) {
    var duration = value(item, 'itunes:duration').split(':');
    return +(duration[0]) * 60 + +(duration[1]);
}

function save_rss(rss) {
    window.localStorage.setItem('rss:last', rss);
}

function restore_rss() {
    var last = window.localStorage.getItem('rss:last');
    if(last) {
        return parseXML(last).children[0].children[0];
    } else {
        return {children: []};
    }
}

function parseXML(xmlStr) {
   return new window.DOMParser().parseFromString(xmlStr, "text/xml");
}

function init_db(storage, state, enable_sync) {
    for(var i = storage.length - 1; i >= 0 ; i--) {
        var key = storage.key(i);

        if(key == 'interface:autoplay') {
            state.autoplay = !!+storage.getItem(key);
        }
        if(key.startsWith('progress:')) {
            var value = JSON.parse(storage.getItem(key));
            var guid = key.slice(9);
        
            if(still_on_page(state, guid)) {
                state.timestamps[guid] = value;
            } else {
                if(old_entry(value['timestamp'])) {
                    // purge old entries
                    storage.removeItem(key);
                }
            }
        }
        if(key == 'sync:auth' && enable_sync) {
            state.sync = storage.getItem(key);
        }
    }
}

function mount(state, rss, rss_elem, tally_elem) {
    mount_html(rss_elem, nodes(rss, 'item').map(item => item_html(state, item)));

    mount_html(tally_elem, Object.entries(state.date).map(tally_html));

    document.getElementById('autoplay').checked = state.autoplay;

    if(!state.sync) {
        document.getElementById('sync-progress').remove();
    }
}

function mount_html(root, fragments) {
    fragments.reverse();
    root.innerHTML = fragments.join('');
}

function item_html(state, item) {
    var guid = value(item, 'guid');
    var episode = state.timestamps[guid];

    return ('<div class="podcast-item"'
            + ' id="item-' + guid + '">'
            + '<div class="podcast-item-image">'
            + image(attr(item, 'itunes:image', 'href'))
            + '</div>'
            + '<div class="podcast-item-heading">'
            + '<a href="' + attr(item, 'enclosure', 'url') + '" '
            + 'onclick="return load(event, _state, \'' + guid + '\')">'
            + '<span class="podcast-item-author">'
            + value(item, 'author')
            + '</span>'
            + ': '
            + '<span class="podcast-item-title">'
            + value(item, 'title')
            + '</span>'
            + '</a>'
            + '</div>'

            + '<div class="podcast-item-details">'
            + '<span class="podcast-item-duration-symbol">'
            + duration_symbol(item_duration(item))
            + '</span>'
            + 'Duration '
            + '<span class="podcast-item-duration">'
            + (value(item, 'itunes:duration')
               || episode_duration(episode))
            + '</span>'
            + '<span class="podcast-item-remaining">'
            + episode_remaining(episode)
            + '</span>'
            + ', added '
            + '<span class="podcast-item-pubdate">'
            + value(item, 'pubDate').substr(0, 16)
            + '</span>'
            + '</div>'
            + '<div class="podcast-item-guid">'
            + guid
            + '</div>'
            + value(item, 'description')
            + episode_progress(episode)
            + priority_controls(state, guid)
            + mark_done_control(state, guid)
            + '</div>');
}

function duration_symbol(duration) {
    if(duration < 20) return '&#9684;';
    if(duration < 35) return '&#9681;';
    if(duration < 50) return '&#9685;';
    return '&#9673;';
}

function image(url) {
    if(url) {
        return '<img src="' + url+ '" />';
    } else {
        return '';
    }
}

function episode_duration(episode) {
    if(episode) {
        return toHHMMSS(episode.duration);
    }
    return '';
}

function episode_remaining(episode) {
    if(episode) {
        var remain_time = episode.currentTime - episode.duration;
        if(episode.currentTime && remain_time) {
            return ' (' + toHHMMSS(remain_time) + ')';
        }
    }
    return '';
}

function episode_progress(episode) {
    if(episode) {
        if(episode.currentTime) {
            var width = (100.0 * episode.currentTime / episode.duration).toFixed(1);
            return '<div class="podcast-item-progress-bar" style="width: ' + width + '%;"></div>';
        }
    }
    return '<div class="podcast-item-progress-bar" style="width: 0%;"></div>';
}

function priority_controls(state, guid) {
    if(state.sync) {
        return ('<span class="set-priority-l" '
                + 'title="Decrease priority" '
                + 'onclick="set_priority(_state, \'' + guid + '\', +1)">\u2BC6</span>'
                + '<span class="set-priority-r" '
                + 'title="Increase priority" '
                + 'onclick="set_priority(_state, \'' + guid + '\', -1)">\u2BC5</span>');
    } else
        return '';
}

function mark_done_control(state, guid) {
    if(state.sync) {
        return ('<span class="mark-done" '
                + 'title="Mark done" '
                + 'onclick="mark_done(_state, \'' + guid + '\')">\uD83D\uDDD1</span>');
    } else
        return '';
}

function tally_html(key_items) {
    return ('<div class="tally-pubdate">'
            + '<span onclick="show_date(_state, \'' + key_items[0] + '\')">'
            + key_items[0]
            + '</span>'
            + tally_html_items('tally-priority0', key_items[1][0])
            + tally_html_items('tally-priority1', key_items[1][1])
            + tally_html_items('tally-priority2', key_items[1][2])
            + '</div>');
}

function tally_html_items(cls, items) {
    return ('<div class="' + cls + '">'
            + items.map(function(item) {
                return ('<span id="tally-' + item.guid + '"'
                        + 'title="' + escapeHtml(item.title) + '" '
                        + 'onclick="show_item(_state, \'' + item.guid + '\')">'
                        + duration_symbol(item.duration)
                        + '</span>');
            }).join('')
            + '</div>');
}

function escapeHtml(html) {
    return (document.createElement('div')
            .appendChild(document.createTextNode(html))
            .parentNode.innerHTML);
}

function show_item(state, guid) {
    state.selected.guid = guid;
    state.selected.date = null;
    render(state);
    scroll_to_guid(state, guid);
}

function show_date(state, pubdate) {
    state.selected.guid = null;
    state.selected.date = pubdate;
    render(state);

    var items = state.date[pubdate];
    for(var i = 0; i < 3; i++) {
        if(items[i].length) {
            scroll_to_guid(state, items[i][0].guid);
        }
    }
}

function scroll_to_guid(state, guid, switch_tab) {
    if(guid) {
        if(switch_tab !== false) {
            show_tab(state, 0);
        }
        document.getElementById('item-' + guid).scrollIntoView();
        window.scrollBy(0, -160);
    }
}

function render(state) {
    compute_class_names(state);
    render_class_names(state);
    render_player(state);
    render_completed(state);
    render_tabs(state);
    render_remaining(state);
}

function dom_needs_update(state, key, value) {
    if(state._dom_text[key] !== value) {
        state._dom_text[key] = value;
        return true;
    } else {
        return false;
    }
}

function compute_class_names(state) {
    state._completed = [];
    state._playing = [];

    Object.entries(state.timestamps).forEach(function(item) {
        var guid = item[0], value = item[1];
        // within 30 seconds of the end
        if(value["duration"] - value["currentTime"] < 30) {
            state._completed.push(guid);
        } else {
            state._playing.push(guid);
        }
    });
}

function render_class_names(state) {
    var class_names = {};

    Object.keys(state.urls).forEach(function(guid) {
        class_names[guid] = ['podcast-item'];
    });

    if(state.selected.guid) {
        class_names[state.selected.guid].push('podcast-item-highlight');
    }

    if(state.selected.date) {
        var items = state.date[state.selected.date];
        for(var i=0; i < items.length; i++) {
            class_names[items[i].guid].push('podcast-item-highlight');
        }
    }

    if(state.playing) {
        class_names[state.playing].push('podcast-item-playing');
    }

    for(var i=0; i < state._completed.length; i++) {
        class_names[state._completed[i]].push('podcast-item-completed');
    }

    for(var i=0; i < state._playing.length; i++) {
        class_names[state._playing[i]].push('podcast-item-inprogress');
    }

    Object.entries(class_names).forEach(function(item) {
        var className = item[1].join(' ');
        if(dom_needs_update(state, 'class_name:' + item[0], className)) {
            document.getElementById('item-' + item[0]).className = className;
            document.getElementById('tally-' + item[0]).className
                = className.replace(/podcast-item( |$)/, '');
        }
    });
}

function render_completed(state) {
    document.getElementById('completed').value = state._completed.join(" ");
}

function render_remaining(state) {
    Object.entries(state.timestamps).forEach(function(item) {
        if(item[1].currentTime) {
            var width = (100.0 * item[1].currentTime / item[1].duration).toFixed(1);
            if(dom_needs_update(state, 'progress:' + item[0], width)) {
                var progress = document.querySelector('#item-' + item[0].replace('.', '\\.') + ' .podcast-item-progress-bar');
                if(progress) {
                    progress.style.width = width + '%';
                }
            }

            var remain_time = item[1].currentTime - item[1].duration;
            if(remain_time) {
                if(dom_needs_update(state, 'remain:' + item[0], remain_time)) {
                    var remaining = document.querySelector('#item-' + item[0].replace('.', '\\.') + ' .podcast-item-remaining');
                    if(remaining) {
                        remaining.innerHTML = ' (' + toHHMMSS(remain_time) + ')';
                    }
                }
            }
        }
    });
}

function render_player(state) {
    var text = audio().paused ? '\u23F8' : '\u23F5';
    if(dom_needs_update(state, 'play_button', text)) {
        state._dom.play_button.innerHTML = text;
    }

    var episode = state.timestamps[state.playing];

    if(episode) {
        var progress_max = Math.floor(episode.duration);
        if(dom_needs_update(state, 'progress_bar_max', progress_max)) {
            state._dom.progress_bar.setAttribute('max', progress_max);
        }
        if(dom_needs_update(state, 'progress_bar_value', episode.currentTime)) {
            state._dom.progress_bar.setAttribute('value', episode.currentTime);
        }

        if(dom_needs_update(state, 'current_time', episode.currentTime)) {
            state._dom.current_time.textContent = toHHMMSS(episode.currentTime);
        }

        var remain_time = toHHMMSS(episode.currentTime - episode.duration);
        if(dom_needs_update(state, 'remain_time', remain_time)) {
            state._dom.remain_time.textContent = remain_time;
        }

        var title = remain_time + ' ' + state.playing;
        if(dom_needs_update(state, 'title', title)) {
            document.title = title;
            document.getElementsByTagName('h1')[0].textContent = title;
        }

        // TODO: show now playing episode title etc.
    }
}

function toHHMMSS(seconds) {
    seconds = parseInt(seconds || 0, 10);

    var prefix = '';
    if(seconds < 0) {
        seconds = - seconds;
        prefix = '-';
    }

    var hours   = Math.floor(seconds / 3600);
    var minutes = Math.floor((seconds - (hours * 3600)) / 60);
    seconds -= (hours * 3600 + minutes * 60);

    if (minutes < 10 && hours > 0) {minutes = "0" + minutes;}
    if (seconds < 10) {seconds = "0" + seconds;}

    return prefix + (hours > 0 ? hours + ':' : '') + minutes + ':' + seconds;
}

function test_toHHMMSS() {
    console.log('toHHMMSS NaN', toHHMMSS(NaN), toHHMMSS(NaN) == '0:00');
    console.log('toHHMMSS zero', toHHMMSS(0), toHHMMSS(0) == '0:00');
    console.log('toHHMMSS 1', toHHMMSS(1), toHHMMSS(1) == '0:01');
    console.log('toHHMMSS 59', toHHMMSS(59), toHHMMSS(59) == '0:59');
    console.log('toHHMMSS 60', toHHMMSS(60), toHHMMSS(60) == '1:00');
    console.log('toHHMMSS 61', toHHMMSS(61), toHHMMSS(61) == '1:01');
    console.log('toHHMMSS 601', toHHMMSS(601), toHHMMSS(601) == '10:01');
    console.log('toHHMMSS 3600-1', toHHMMSS(3600-1), toHHMMSS(3600-1) == '59:59');
    console.log('toHHMMSS 3600', toHHMMSS(3600), toHHMMSS(3600) == '1:00:00');
    console.log('toHHMMSS -1', toHHMMSS(-1), toHHMMSS(-1) == '-0:01');
    console.log('toHHMMSS -61', toHHMMSS(-61), toHHMMSS(-61) == '-1:01');
    console.log('toHHMMSS -3661', toHHMMSS(-3661), toHHMMSS(-3661) == '-1:01:01');
}

function render_tabs(state) {
  state.tabs.body.forEach(function(tab, i) {
    tab.style.display = i == state.tabs.active ? 'block' : 'none';
  });
}

function show_tab(state, active) {
  state.tabs.active = active;
  render_tabs(state);
  return false;
}

function audio() {
    return _state._dom.player;
}

function load(event, state, guid) {
    if(event) {
        event.preventDefault();
        event.stopPropagation();
    }

    state.playing = guid;

    if(audio().src != state.urls[guid]) {
        audio().src = state.urls[guid];
        audio().currentTime = get_progress(state, guid);
    }

    play(state);

    return false;
}

function play(state) {
    if(audio().src) {
        state._dom.play_button.focus();

        if(audio().paused) {
            audio().play();
        } else {
            audio().pause();
        }

    } else {
        resume(state) || next(state);
    }
}

function forward() {
    if(audio().src) {
        if(audio().duration - audio().currentTime > 30) {
            audio().currentTime += 30;
        }
        audio().play();
    }
}

function rewind() {
    if(audio().src) {
        if(audio().currentTime > 10) {
            audio().currentTime -= 10;
        } else {
            audio().currentTime = 0;
        }
        audio().play();
    }
}

function done() {
    if(audio().src && audio().currentTime > 30 && audio().duration) {
        audio().currentTime = audio().duration;
        audio().pause();
        log_progress.bind(audio())();
    }
}

function resume(state) {
    var recent = [];
    Object.entries(state.timestamps).forEach(function(item) {
        recent.push([item[1].timestamp, item[0]]);
    });
    if(recent.length) {
        recent.sort();
        var guid = recent[recent.length - 1][1];
        load(null, state, guid);
        scroll_to_guid(state, guid, false);
        return true;
    } else {
        return false;
    }
}

function speed(button) {
    var speeds = ['0.75x', '1x', '1.3x', '2x', '1.3x ', '1x '];
    var idx = speeds.indexOf(button.textContent);
    idx = (idx + 1) % speeds.length;
    button.textContent = speeds[idx];
    audio().playbackRate = parseFloat(speeds[idx].replace('x', ''));
}

function scrub(event) {
    audio().currentTime = (Math.floor(audio().duration)
                           * (event.offsetX / event.target.offsetWidth));
    audio().play();
}



var key_map = {
  ' ': document.querySelector('.podcast-play'),
  'ArrowLeft': document.querySelector('.podcast-rewind'),
  'ArrowRight': document.querySelector('.podcast-forward'),
  ',': document.querySelector('.podcast-rewind'),
  '.': document.querySelector('.podcast-forward'),
  'n': document.querySelector('.podcast-next'),
  's': document.querySelector('.podcast-speed'),
};

document.addEventListener('keydown', function(e) {
  if(e.srcElement.type == 'textarea') return;

  var button = key_map[e.key];
  if(button !== undefined) {
    e.preventDefault();
    button.dispatchEvent(new MouseEvent('click'));
  }
}, true);

function autoplay(state) {
    state.autoplay = document.getElementById('autoplay').checked;

    window.localStorage.setItem('interface:autoplay',
                                state.autoplay ? '1' : '0');
}

audio().addEventListener('ended', function() {
    if(_state.autoplay) {
        next(_state);
    }
});

function next(state) {
    // sort by priority order
    var guids = Object.keys(state.urls).sort(function(a, b) {
        var a_key = get_priority(state, a), b_key = get_priority(state, b);
        return a_key == b_key ? 0 : a_key < b_key ? 1 : -1;
    });

    var n = guids.indexOf(state.playing);
    if(n < 0) { n = guids.length; }
    var guid = guids[n-1];
    while(guid) {
        if(state._completed.indexOf(guid) == -1) {
            load(null, state, guid);
            scroll_to_guid(state, guid, false);
            return;
        }
        n--;
        guid = guids[n-1];
    }
}

function still_on_page(state, guid) {
    return state.urls[guid] !== undefined;
}

function old_entry(entry_timestamp) {
    return timestamp() - entry_timestamp > 60*60*24;
}

audio().addEventListener('loadedmetadata', log_progress);
audio().addEventListener('timeupdate', log_progress);
audio().addEventListener('ended', log_progress);

function log_progress() {
    var value = {"currentTime": Math.floor(this.currentTime),
                 "duration": Math.floor(this.duration),
                 "timestamp": timestamp()};

    _state.timestamps[_state.playing] = value;

    write_progress(_state._progress, _state.playing, value);

    render(_state);
}

function get_progress(state, guid) {
    var value = state.timestamps[guid];
    if(value) {
        // not completed
        if(value["currentTime"] < value["duration"]) {
            return value["currentTime"];
        }
    }
    return 0;
}

function timestamp() {
    return Math.floor(Date.now() / 1000);
}

function write_progress(progress, guid, value) {
    if(progress.guid == guid) {
        progress.value = value;
    } else {
        flush_progress(progress);

        queue_progress(progress, guid, value,
                       window.setTimeout(flush_progress, 5000, progress));
    }
}

function flush_progress(progress) {
    window.clearTimeout(progress.timeout);

    if(progress.guid) {
        save_progress(progress.guid, progress.value);
    }

    queue_progress(progress, null, null, null);
}

function queue_progress(progress, guid, value, timeout) {
    progress.guid = guid;
    progress.value = value;
    progress.timeout = timeout;
}

function save_progress(guid, value) {
    window.localStorage.setItem('progress:' + guid, JSON.stringify(value));
}

function input_auth(state) {
    var auth = window.prompt('sync progress.json uid:pwd');
    if(auth) {
        state.sync = window.btoa(auth);
        window.localStorage.setItem('sync:auth', state.sync);
        window.location.reload();
    }
}

var _progress_url = '/progress/progress.json';
var _completed_url = '/progress/completed.txt';

function sync_progress(state) {
    if(state.sync) {
        state._dom.sync_status.textContent = "Syncing...";

        var oGET = new XMLHttpRequest();
        oGET.addEventListener("load", on_load);
        oGET.open("GET", _progress_url);
        oGET.responseType = 'json';
        oGET.setRequestHeader("Authorization", "Basic " + state.sync);
        oGET.send();

        function on_load() {
            if(oGET.response) {
                merge_progress(state, oGET.response);
            }
            compute_class_names(state);
            put_data(state, _progress_url,
                     JSON.stringify(get_progress_timestamps(state)));
            put_data(state, _completed_url, state._completed.join(" "));
            render(state);

            state._dom.sync_status.textContent = "Completed.";
            window.setTimeout(function(){
                state._dom.sync_status.textContent = "";
            }, 3000);
        }
    }
}

function merge_progress(state, episodes) {
    episodes.forEach(function(episode) {
        if(still_on_page(state, episode.guid)) {
            if(state.timestamps[episode.guid] !== undefined) {
                var our = state.timestamps[episode.guid];
                if(episode.timestamp >= our.timestamp
                   && episode.currentTime >= our.currentTime) {
                    our.currentTime = episode.currentTime;
                    our.timestamp = episode.timestamp;
                }
            } else {
                state.timestamps[episode.guid] = {"currentTime": episode.currentTime,
                                                  "duration": episode.duration,
                                                  "timestamp": episode.timestamp};
            }
            save_progress(episode.guid, state.timestamps[episode.guid]);
        }
    });
}

function put_data(state, url, data) {
    if(state.sync) {
        var oPUT = new XMLHttpRequest();
        oPUT.open("PUT", url);
        oPUT.setRequestHeader("Authorization", "Basic " + state.sync);
        oPUT.send(data);
    }
}

function get_progress_timestamps(state) {
    var merged = [];

    Object.entries(state.timestamps).forEach(function(item) {
        merged.push({
            // episode filename
            "guid": item[0],
            // playback position (seconds)
            "currentTime": item[1].currentTime,
            // episode total duration (seconds)
            "duration": item[1].duration,
            // unix seconds since epoch
            "timestamp": item[1].timestamp});
    });

    merged.sort(function(a, b) {
        if (a.guid < b.guid) return -1;
        if (a.guid > b.guid) return 1;
        if (a.timestamp < b.timestamp) return -1;
        if (a.timestamp > b.timestamp) return 1;
        return 0;
    });

    return merged;
}

function make_priority(items) {
    var result = [[], [], []];
    for(var i = 0; i < items.length; i++) {
        if(items[i][0] >=0 && items[i][0] <= 2)
            result[items[i][0]].push(items[i][1]);
    }
    return result;
}

function map_priority(priority, f) {
    var result = [];
    for(var i = 0; i < 3; i++) {
        for(var j = 0; j < priority[i].length; j++) {
            result.push(f(i, priority[i][j]));
        }
    }
    return make_priority(result);
}

function get_priority(state, guid) {
    for(var i = 0; i < 3; i++) {
        var p = state.priority[i];
        for(var j = 0; j < p.length; j++) {
            if(matches_guid(p[j], guid))
                return i;
        }
    }
    return 1;
}

function matches_guid(p, guid) {
    if(is_regexp(p)) {
        if(matches_guid.cache[p] === undefined) {
            matches_guid.cache[p] = new RegExp(p.substr(1));
        }
        if(matches_guid.cache[p].test(guid))
            return true;
    } else if(p == guid)
        return true;
    return false;
}
matches_guid.cache = {};

function is_regexp(p) {
    return p[0] == '/';
}

var _priority_url = '/progress/priority.json';

function sync_priority(state) {
    if(state.sync) {
        var oGET = new XMLHttpRequest();
        oGET.addEventListener("load", on_load);
        oGET.open("GET", _priority_url);
        oGET.responseType = 'json';
        oGET.setRequestHeader("Authorization", "Basic " + state.sync);
        oGET.send();

        function on_load() {
            if(oGET.response) {
                state.priority = purge_priority(state, oGET.response);
            }
            render_priority(state);
        }
    }
}

function purge_priority(state, priority) {
    return map_priority(priority, (i, p) => {
        if(is_regexp(p) || still_on_page(state, p)) {
            return [i, p];
        } else
            return [-1, p];
    });
}

function render_priority(state) {
    update_tally_priority(state);
    mount_html(document.getElementById('tally'),
               Object.entries(state.date).map(tally_html).reverse());

    sort_by_priority(state);
    render(state);
}

function update_tally_priority(state) {
    Object.entries(state.date).forEach(function(key_items) {
        state.date[key_items[0]]
            = map_priority(key_items[1],
                           (i, p) => {
                               return [get_priority(state, p["guid"]), p];
                           });
    });
}

function sort_by_priority(state) {
    var items = make_priority(Object.keys(state.urls).map(guid => {
        var e = document.getElementById('item-' + guid);
        e.remove();
        return [get_priority(state, guid), e];
    }));

    for(var i = 0; i < 3; i++) {
        var p = document.getElementById('priority' + i);

        for(var j = items[i].length - 1; j >= 0; j--)
            p.appendChild(items[i][j]);
    }
}

function set_priority(state, guid, delta) {
    var i = get_priority(state, guid);
    var p = state.priority[i];
    p.splice(p.indexOf(guid), 1);
    if(delta < 0) {
        state.priority[i + delta].push(guid);
    } else {
        state.priority[i + delta].unshift(guid);
    }

    render_priority(state);

    put_data(state, _priority_url, JSON.stringify(state.priority));
}

function mark_done(state, guid) {
    var value = {"currentTime": 24 * 60 * 60 - 1,
                 "duration": 24 * 60 * 60 - 1,
                 "timestamp": timestamp()};

    state.timestamps[guid] = value;

    write_progress(state._progress, guid, value);

    render(state);
}

audio().addEventListener('error', function() {
    logError("Error " + this.error.code + "; "
             + "details: " + this.error.message + "\n");
});

function logError(msg) {
    _state._dom.errors.innerHTML += msg;
}
