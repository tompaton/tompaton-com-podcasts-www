#!/bin/bash
mkdir -p /data/progress /data/temp_nginx
chgrp -R www-data /data/progress && chmod -R g+ws /data/progress
nginx -g "daemon off;"
