FROM nginx
EXPOSE 80

RUN usermod -aG www-data nginx

RUN mkdir -p /html /data/ \
             /data/downloaded /data/images /data/progress /data/stats \
             /data/proxy_rss /data/temp_nginx

RUN ln -s /data/index.html /html/index.html && \
    ln -s /data/downloaded.rss /html/downloaded.rss && \
    ln -s /data/processed /html/downloaded && \
    ln -s /data/images /html/images && \
    ln -s /data/proxy_rss /html/proxy_rss && \
    ln -s /data/stats /html/stats

COPY default.conf /etc/nginx/conf.d/

COPY html /html

COPY entrypoint.sh .

ENTRYPOINT ["./entrypoint.sh"]
